// @Grab('org.gitlab4j:gitlab4j-api:4.14.17')
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.MergeRequestFilter
import org.gitlab4j.api.Constants.MergeRequestState;

def getProjects(String gitlabUrl, String token){
    sh "export"
    echo "$env.BUILD_ID"
    GitLabApi gitlabApi = new GitLabApi(gitlabUrl, token)
    println "$gitlabApi"
    def projectApi = gitlabApi.getProjectApi()
    println "projectApi: ${projectApi}"
    def projects = projectApi.getProjects(10);
    println "projects: ${projects}"

    projects.each{
        println "$it"
    }
    }

@NonCPS
def createFeatureMR(String gitlabUrl, String token, Object projectId, String sourceBranch,String targetBranch="develop", String title ="WIP: feature to develop", String description=null, Integer assigneeId=null) {
    GitLabApi gitlabApi = new GitLabApi(gitlabUrl, token)
    def mrApi = gitlabApi.getMergeRequestApi()

    def existingMRFilter = new MergeRequestFilter()
        .withProjectId(projectId)
        .withSourceBranch(sourceBranch)
        .withTargetBranch(targetBranch)
        .withState(MergeRequestState.OPENED)

    def anyExisingMr = mrApi.getMergeRequests(existingMRFilter)
    print "$anyExisingMr"
    if(anyExisingMr){        
        println "MR already exists"
        return        
    }
    mrApi.createMergeRequest(projectId, sourceBranch, targetBranch, title, description, assigneeId)
    // def project = gitlabApi.getProjectApi().getProjects(projectName)
    // println "$projects"

    

}